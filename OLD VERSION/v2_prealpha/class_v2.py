import requests
import time
import threading
# import winsound


class reminder():
    def __init__(self):
        url_eos = 'https://www.okb.com/v2/spot/markets/tickers'
        url_usd2cny = 'https://www.okb.com/v2/futures/market/indexTickerAll.do'
        headers_usd2cny = {
            'Host': 'www.okb.com',
            'Referer': 'https://www.okb.com/marketList',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
            'Cookie': '_ga=GA1.2.572728249.1530522288; _gid=GA1.2.1509264208.1530522288; perm=A364DD49D354EDA46B23976B747C8C85; lp=; Hm_lvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530522289; __zlcmid=nChSZMJiQJtcna; ref=https://www.okb.com/marketList; first_ref=https://www.okb.com/marketList; locale=en_US; _gat_gtag_UA_115738092_1=1; Hm_lpvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530583975'
        }
        headers_eos = {
            'Host': 'www.okb.com',
            'Referer': 'https://www.okb.com/marketList',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'X-Requested-With': 'XMLHttpRequest',
            'Cookie': '_ga=GA1.2.572728249.1530522288; _gid=GA1.2.1509264208.1530522288; perm=A364DD49D354EDA46B23976B747C8C85; lp=; Hm_lvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530522289; __zlcmid=nChSZMJiQJtcna; ref=https://www.okb.com/marketList; first_ref=https://www.okb.com/marketList; locale=en_US; _gat_gtag_UA_115738092_1=1; Hm_lpvt_b4e1f9d04a77cfd5db302bc2bcc6fe45=1530543498'
        }

    def get_page(self):
        try:
            response = requests.get(self.url_eos, headers=self.headers_eos)
            if response.status_code == 200:
                return response.json()
        except requests.ConnectionError as e:
            print('Error', e.args)

    def get_usd2cny(self):
        try:
            response = requests.get(self.url_usd2cny, headers=self.headers_usd2cny)
            if response.status_code == 200:
                return response.json()
        except requests.ConnectionError as e:
            print('Error', e.args)

    def parse_page(json):
        if json:
            items = json.get('data')
            item_exist = False
            for i in range(5,7):
                item = items[i]  # 52 or 53
                if item.get('symbol') == 'eos_usdt':
                    item_exist = True
                    # print(i)
                    info = {}
                    info['index'] = i
                    info['symbol'] = item.get('symbol')
                    info['buy'] = item.get('buy')
                    info['sell'] = item.get('sell')
                    break
            if not item_exist:
                print('EOS INDEX SEARCH FAILED!')
        return info

    def parse_usd2cny(json):
        if json:
            items = json.get('data')
            item = items[0]
            usdCnyRate = item['usdCnyRate']
        return float(usdCnyRate)

    def job(self, info):
        content_eos = self.get_page()
        info_dict = self.parse_page(content_eos)
        rate_info = self.get_usd2cny()
        rate = self.parse_usd2cny(rate_info)
        buy = float(info_dict['buy']) * rate
        sell = float(info_dict['sell']) * rate
        print('currency='+info_dict['symbol']+'  buy=￥'+str(buy)+'  sell=￥'+str(sell))
        if sell > info['high']:
            print('sell > high')
            # winsound.Beep(1000, 1000)
        if sell < info['low']:
            print('sell < low')
            # winsound.Beep(1000, 1000)

    def run(self, interval, info, wait=True):
        base_time = time.time()
        next_time = 0
        while True:
            t = threading.Thread(target=self.job, args=info)
            t.daemon = True
            self.t.start()
            if wait:
                self.t.join()
            next_time = ((base_time - time.time()) % interval) or interval
            time.sleep(next_time)
