import sys
import getopt
from class_v2 import reminder

args = sys.argv

if __name__ == '__main__':
    seconds = 0
    low = 0
    high = 0
    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:l:h:", ["time=", "low=", "high="])
    except getopt.GetoptError:
        print('python3 okb_reminder_v*.py -t <time(s)> -l <low price> -h <high price>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-t', '--time'):
            seconds = int(arg)
        elif opt in ('-l', '--low'):
            low = float(arg)
        elif opt in ('-h', '--high'):
            high = float(arg)
    print('time=', seconds, 'low=', low, 'high=', high)
    infomation = {}
    infomation['low'] = low
    infomation['high'] = high
    reminder.run(seconds, infomation, True)
