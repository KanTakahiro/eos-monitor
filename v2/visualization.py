import plotly.offline as py
import plotly.graph_objs as go
import pandas as pd
import configparser

config = configparser.ConfigParser()
config.read('config', encoding='utf-8')
low_price = float(config['DEFAULT']['LowPrice'])
high_price = float(config['DEFAULT']['HighPrice'])
df = pd.read_csv('log.csv')
trace_data = go.Scatter(x=df.time, y=df.price, name='price')
trace_high = go.Scatter(
    x=df.time, y=[high_price for i in range(len(df.price))], name='high')
trace_low = go.Scatter(
    x=df.time, y=[low_price for i in range(len(df.price))], name='low')
data = [trace_data, trace_high, trace_low]
py.plot(data, filename='time-price.html')
