import time
import datetime
import configparser
from class_parser import Parser
from class_mail import Mail
import csv

config = configparser.ConfigParser()
config.read('config', encoding='utf-8')
low_price = float(config['DEFAULT']['LowPrice'])
high_price = float(config['DEFAULT']['HighPrice'])
is_loop = config['DEFAULT'].getboolean('Loop')
time_sleep = int(config['DEFAULT']['TimeSleep'])
print('区间低值：', low_price)
print('区间高值：', high_price)
print('是否循环：', is_loop)
print('时间间隔（秒）：', time_sleep)
print()
parser = Parser()
m = Mail()
price_history = []
time_history = []
log = open('log.csv', 'w+')
log.write('\"time\",\"price\"\n')
log.flush()
while is_loop:
    current_price = parser.parse_page(parser.get_page())
    if current_price > 0:
        now = datetime.datetime.now()
        price_history.append(current_price)
        time_history.append(now)
        log_writer = csv.writer(log)
        log_writer.writerow([now, current_price])
        log.flush()
        print('当前时间：', now)
        print('当前价格：¥', current_price)
        if current_price < low_price:
            content = '当前价格：¥'+str(current_price) + \
                                   '\n低于预设价格区间低值：'+str(low_price)
            m.mail(content)
        if current_price > high_price:
            content = '当前价格：¥'+str(current_price) + \
                                   '\n低于预设价格区间高值：'+str(high_price)
            m.mail(content)
        print()
        time.sleep(time_sleep)
    else:
        print('程序将在等待10min后重新开始执行')
        time.sleep(600)
        print('等待结束，重新开始执行')
